/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { FC } from 'react';
import { Provider } from 'react-redux'
import Navigation from './src/navigation'
import store from './src/flux/store'

const App: FC = () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
};

export default App;
