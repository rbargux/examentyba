import React, { useEffect, useState } from 'react'
import { Text, View, ScrollView } from 'react-native'
import { ZOOMATO_API } from '../../constant/zoomato'
import CardCity from '../../components/CardCity'

const ListRestaurant = (props: any) => {
    const [establishments, setEstablishments] = useState([])
    const { id = 0 } = { ...props.navigation.state.params }
    useEffect(() => {
        const getRestaurantes = async (id: number) => {
            const url = `https://developers.zomato.com/api/v2.1/establishments?city_id=${id}`
            fetch(url, {
                headers: {
                    'user-key': ZOOMATO_API,
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    setEstablishments(response.establishments)
                });

        }
        getRestaurantes(id)
    }, [])

    const handleEstablisment = (id: number) => {
        props.navigation.push('DetailRestaurant', { id })
    }

    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <ScrollView>
                {
                    establishments.map((esta: any) => {
                        const establecimiento = esta.establishment
                        const { id = 0, name = '' } = { ...establecimiento }
                        return (<CardCity key={`${id}`} id={id} name={name} onPress={handleEstablisment} />)
                        // 
                    })
                }
            </ScrollView>
        </View>

    )
}

ListRestaurant.navigationOptions = {
    title: 'Restaurantes'
}

export default ListRestaurant