import React, { useEffect } from 'react';
import {
    ActivityIndicator,
    StatusBar,
    StyleSheet,
    View,
    Text
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

const AuthLoadingScreen = (props: any) => {

    useEffect(() => {
        const _bootstrapAsync = async () => {
            const userToken = await AsyncStorage.getItem('@userLogin');
            props.navigation.navigate(userToken ? 'App' : 'Auth');
        };
        _bootstrapAsync();
    }, []);

    return (
        <View>
            <ActivityIndicator />
            <StatusBar barStyle="default" />
        </View>
    );
}

export default AuthLoadingScreen