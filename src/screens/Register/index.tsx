import React, { useState } from 'react'
import { Text, View } from 'react-native'
import InputCustom from '../../components/InputCustom'
import TouchableCustom from '../../components/TouchableCustom'
import { agregarUsuario, agregarUsuarioExito, agregarUsuarioError } from '../../flux/actions/registerActions'
import { useDispatch } from 'react-redux';
import Styles from './styles'

const Register = (props: any) => {
    const [user, setUser] = useState('')
    const [password, setPassword] = useState('')
    const { container } = { ...Styles };

    const dispatch = useDispatch();

    const register = async () => {
        if (!!user && !!password) {
            try {
                dispatch(agregarUsuario())
                dispatch(agregarUsuarioExito({ user, password }))
                props.navigation.navigate('Login')
            } catch (error) {
                dispatch(agregarUsuarioError())
                console.log('error', error)
            }
        }
        else {
            console.log('Ambos campos deben ir diligenciados')
        }
    }
    return (
        < View style={container} >
            <Text style={{ fontSize: 45 }}>Registro</Text>
            <InputCustom title='Nombre de Usuario' value={user} placeHolder='Usuario' onChangeText={setUser} />
            <InputCustom title='Contraseña' value={password} placeHolder='Contraseña' secureTextEntry={true} onChangeText={setPassword} />
            <TouchableCustom onPress={register} title='Registrarme' />
            {/* <TouchableCustom onPress={() => props.navigation.push('Register')} title='Registrarse' /> */}
        </View >
    )
}

Register.navigationOptions = {
    title: 'Register'
};

export default Register