import React, { useState } from 'react';
import { View, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import InputCustom from '../../components/InputCustom'
import TouchableCustom from '../../components/TouchableCustom'
import { useSelector } from 'react-redux';
import Styles from './styles';


const Login = (props: any) => {
  const { navigation = {} } = { ...props }
  const [user, setUser] = useState('')
  const [password, setPassword] = useState('')
  const { container } = { ...Styles };
  const _userRegister = useSelector((state: any) => state.registerReducer.users)

  const validateUser = async () => {
    if (!!user && !!password) {
      const [almostOne] = [..._userRegister]
      if (!!almostOne) {
        const consulta: any = _userRegister.find((_user: any) => _user.user == user && _user.password == password)
        const keysLength = Object.keys(consulta).length
        const success = keysLength > 0 ? true : false
        if (success) {
          try {
            await AsyncStorage.setItem('@userLogin', JSON.stringify({ user }))
            navigation.navigate('App')
          } catch (e) {
            console.log('error AsyncStorage', e)
          }
        }
        else {
          console.log('Alguno de los campos es incorrecto')
        }
      } else {
        console.log('No hay usuarios creados')
      }
    }
    else {
      console.log('Ambos campos deben ser diligenciados')
    }

  }
  return (
    < View style={container} >
      <Text style={{ fontSize: 45 }}>Bienvenidos</Text>
      <InputCustom title='Nombre de Usuario' value={user} placeHolder='Usuario' onChangeText={setUser} />
      <InputCustom title='Contraseña' value={password} placeHolder='Contraseña' secureTextEntry={true} onChangeText={setPassword} />
      <TouchableCustom onPress={validateUser} title='Login' />
      <TouchableCustom onPress={() => props.navigation.push('Register')} title='Registrarse' />
    </View >
  );
};

Login.navigationOptions = {
  header: null,
};

export default Login;
