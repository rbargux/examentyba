import React, { useEffect, useState } from 'react'
import { Text, View, ScrollView } from 'react-native'
import { ZOOMATO_API } from '../../constant/zoomato'

const DetailRestaurant = (props: any) => {
    const [detail, setDetail] = useState({})
    const { id = 0 } = { ...props.navigation.state.params }
    useEffect(() => {
        const getDetail = async (id: number) => {
            const url = `https://developers.zomato.com/api/v2.1/restaurant?res_id=${id}`
            fetch(url, {
                headers: {
                    'user-key': ZOOMATO_API,
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    setDetail(response)
                });
        }
        getDetail(id)
    }, [])

    const { name = '', price_range = 0, location = {} } = { ...detail }
    const { address = '', locality = '', city = '' } = { ...location }
    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <View style={{ width: 300, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
                <Text>Name</Text>
                <Text>{name}</Text>
            </View>
            <View style={{ width: 300, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
                <Text>Price Range</Text>
                <Text>{price_range}</Text>
            </View>
            <View style={{ width: 300, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
                <Text>locality</Text>
                <Text>{locality}</Text>
            </View>
            <View style={{ width: 300, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
                <Text>city</Text>
                <Text>{city}</Text>
            </View>
        </View>

    )
}

DetailRestaurant.navigationOptions = {
    title: 'Detalle'
}

export default DetailRestaurant