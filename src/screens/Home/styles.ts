import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    searchInput: {
        width: 300,
        borderBottomWidth: 1,
        borderBottomColor: 'gray'
    }
})