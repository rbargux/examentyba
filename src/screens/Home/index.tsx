import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import CardCity from '../../components/CardCity'
import Styles from './styles'


const ciudades = [{
    id: 280,
    name: "New York City, NY"
},
{
    id: 1000,
    name: "Long Island, NY"
},
{
    id: 1012,
    name: "Buffalo, NY",
}
]

const Home = (props: any) => {
    const [search, setSearch] = useState('')
    const { container, searchInput } = { ...Styles }
    const handleCity = (id: number) => {
        props.navigation.push('ListRestaurant', { id })
    }

    const deslogear = () => {
        console.log(1)
        AsyncStorage.clear()
        props.navigation.navigate('Auth')
    }

    return (
        <View style={container}>
            <TextInput value={search} onChangeText={(e) => setSearch(e)} style={searchInput} placeholder={'Digite una Ciudad'} />
            {
                ciudades.filter((ciudad) => ciudad.name.includes(search)).map((ciudad) => {
                    return (
                        <CardCity key={`${ciudad.id}`} name={ciudad.name} id={ciudad.id} onPress={handleCity} />
                    )
                })
            }
            <TouchableOpacity onPress={() => deslogear()} style={{ backgroundColor: 'red', justifyContent: 'center', padding: 20, width: 300, height: 20 }}>
                <Text style={{ color: 'white', textAlign: 'center' }}>Deslogear</Text>
            </TouchableOpacity>
        </View>
    )
}

Home.navigationOptions = {
    header: null
}

export default Home;