import React, { FC } from 'react';
import { TouchableOpacity, Text } from 'react-native'
import { iTouchableCustom } from './interface'
import { hitStop } from '../../constant/components'
import Styles from './styles'

const { container, text } = { ...Styles }

const TouchableCustom: FC<iTouchableCustom> = ({ title = '', onPress }) => {
    return (
        <TouchableOpacity onPress={() => onPress()} style={container} hitSlop={hitStop}>
            <Text style={text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TouchableCustom

