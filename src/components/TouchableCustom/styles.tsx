import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        backgroundColor: 'green',
        width: 300,
        padding: 10,
        borderRadius: 5
    },
    text: {
        textAlign: "center",
        color: 'white',
        fontSize: 16
    }
})