interface iTouchableCustom {
    title: string,
    onPress(): any
}

export { iTouchableCustom }