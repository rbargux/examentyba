interface iCardCity {
    id: number,
    name: string,
    onPress(id: number): any
}

export { iCardCity }