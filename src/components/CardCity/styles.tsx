import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 5,
        padding: 5,
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        width: 300,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})