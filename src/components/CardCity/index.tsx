import React from 'react';
import { TouchableOpacity, Text } from 'react-native'
import Styles from './styles'
import { iCardCity } from './interface'
import { hitStop } from '../../constant/components'

const CardCity = ({ id = 0, name = '', onPress }: iCardCity) => {
    const { container } = { ...Styles }
    return (
        <TouchableOpacity onPress={() => onPress(id)} style={container} hitSlop={hitStop}>
            <Text>{`id : ${id}`}</Text>
            <Text>{`nombre : ${name}`}</Text>
        </TouchableOpacity>)
}

export default CardCity