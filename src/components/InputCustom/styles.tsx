import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flexDirection: "column",
        alignItems: 'flex-start'
    },
    textInput: {
        height: 40,
        width: 300,
        borderBottomWidth: 1,
        borderBottomColor: 'gray'
    }
})