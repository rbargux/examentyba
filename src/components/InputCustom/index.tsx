import React, { FC } from 'react';
import { View, Text, TextInput } from 'react-native'
import { iInputCustom } from './interface'
import Styles from './styles'

const { container, textInput } = { ...Styles }

const InputCustom: FC<iInputCustom> = ({ title = '', value = '', placeHolder = '', secureTextEntry = false, onChangeText }) => {
    return (
        <View style={container}>
            <Text>{title}</Text>
            <TextInput onChangeText={(e) => onChangeText(e)} value={value} style={textInput} placeholder={placeHolder} secureTextEntry={secureTextEntry} />
        </View>
    )
}

export default InputCustom;