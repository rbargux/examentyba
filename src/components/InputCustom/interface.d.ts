interface iInputCustom {
    title: string,
    value: string,
    placeHolder: string,
    secureTextEntry?: boolean,
    onChangeText(e: any): any
}

export { iInputCustom }