import {
    REGISTRAR_USUARIO, REGISTRAR_USUARIO_ERROR, REGISTRAR_USUARIO_EXITO
} from '../types/registerTypes';

export const agregarUsuario = () => ({
    type: REGISTRAR_USUARIO
});

export const agregarUsuarioExito = (user: any) => ({
    type: REGISTRAR_USUARIO_EXITO,
    payload: user
});

export const agregarUsuarioError = () => ({
    type: REGISTRAR_USUARIO_ERROR
});