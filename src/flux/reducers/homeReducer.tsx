const initialState = {
    productos: [],
    error: false,
    loading: false
}

export default (state: any = initialState, action: any) => {
    switch (action.type) {
        case 'TEST_HOME':
            return {
                ...state,
                error: true
            }
        default:
            return state;
    }
}