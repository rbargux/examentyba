import { combineReducers } from 'redux';
import homeReducer from './homeReducer';
import registerReducer from './registerReducer'

export default () => combineReducers({
    homeReducer: homeReducer,
    registerReducer: registerReducer
});