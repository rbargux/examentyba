import { REGISTRAR_USUARIO, REGISTRAR_USUARIO_ERROR, REGISTRAR_USUARIO_EXITO } from '../types/registerTypes';

const initialState = {
    error: false,
    loading: false,
    users: []
}

export default (state = initialState, action: any) => {
    switch (action.type) {
        case REGISTRAR_USUARIO:
            return {
                ...state,
                error: false,
                loading: true
            }
        case REGISTRAR_USUARIO_ERROR:
            return {
                ...state,
                error: true,
                loading: false
            }
        case REGISTRAR_USUARIO_EXITO:
            return {
                ...state,
                error: false,
                loading: false,
                users: [...state.users, action.payload]

            }
        default:
            return state;
    }
}