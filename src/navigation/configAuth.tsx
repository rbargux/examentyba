import Login from '../screens/Login'
import Register from '../screens/Register'

export const StackNavigatorConfigAuth: any = {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: '#FF681F'
        },
        headerTintColor: '#222',
        headerTitleStyle: {
            fontWeight: 'bold'
        }
    }
}

export const RouteConfigsAuth = {
    Login: {
        screen: Login
    },
    Register: {
        screen: Register
    }
}


