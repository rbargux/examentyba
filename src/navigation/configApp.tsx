import Home from '../screens/Home'
import ListRestaurant from '../screens/ListRestaurant'
import DetailRestaurant from '../screens/DetailRestaurant'

export const StackNavigatorConfigApp: any = {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: '#FF681F'
        },
        headerTintColor: '#222',
        headerTitleStyle: {
            fontWeight: 'bold'
        }
    }
}

export const RouteConfigsApp = {
    Home: {
        screen: Home
    },
    ListRestaurant: {
        screen: ListRestaurant
    },
    DetailRestaurant: {
        screen: DetailRestaurant
    }
}