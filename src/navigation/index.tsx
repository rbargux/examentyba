import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { RouteConfigsApp, StackNavigatorConfigApp } from './configApp'
import { RouteConfigsAuth, StackNavigatorConfigAuth } from './configAuth'
import AuthLoadingScreen from '../screens/AuthLoadingScreen'

const AppStack = createStackNavigator(RouteConfigsApp, StackNavigatorConfigApp);
const AuthStack = createStackNavigator(RouteConfigsAuth, StackNavigatorConfigAuth);

export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading: AuthLoadingScreen,
            App: AppStack,
            Auth: AuthStack,
        },
        {
            initialRouteName: 'AuthLoading',
        }
    )
);